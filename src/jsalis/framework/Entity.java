/**
 * The Entity interface is used by Engine.
 * Any class controlled by an Engine must implement this interface.
 * 
 * @author John Salis
 */

package jsalis.framework;

import java.awt.Graphics2D;

public interface Entity {
	
	public void tick();
	
	public void render(Graphics2D g);
	
}