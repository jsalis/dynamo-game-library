/**
 * Represents a playable audio clip.
 * 
 * @author John Salis
 */

package jsalis.framework;

import java.applet.AudioClip;
import loader.ResourceLoader;

public class Sound {
	
	private String name;
	private AudioClip sound;
	
	public Sound(String filePath) {
		this.name = filePath.substring(0, filePath.lastIndexOf("."));
		try {
			sound = ResourceLoader.getAudioClip(filePath);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void play() {
		if(sound != null) {
			sound.play();
		}
	}
	
	
	public void loop() {
		if(sound != null) {
			sound.loop();
		}
	}
	
	
	public void stop() {
		if(sound != null) {
			sound.stop();
		}
	}
	
	
	public String getName() {
		return name;
	}
	
}
