/**
 * Types of game states used by StateManager.
 * 
 * @author John Salis
 */

package jsalis.framework;

public enum StateType {
	MENU, GAME, PAUSE, LOAD, SHOP, SCOREBOARD, CUTSCENE, EDITOR, CREDITS
}
