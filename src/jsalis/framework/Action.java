/**
 * Types of actions that can be performed.
 * An action is a way that the user interacts with the program.
 * 
 * @author John Salis
 */

package jsalis.framework;

public enum Action {
	UP, DOWN, LEFT, RIGHT, FIRE,
	PAUSE
}
