/**
 * Manages the list of states within the game.
 * Controls the game engine and handles changes
 * between different states. When a state is changed
 * the reference to the current state within the Engine
 * is updated.
 * 
 * @author John Salis
 */

package jsalis.framework;

import java.awt.Canvas;
import java.awt.Container;
import java.util.ArrayList;

public class StateManager {
	
	private Engine engine;
	private ArrayList<AbstractState> stateList = new ArrayList<AbstractState>();
	private StateType currentState;
	
	public StateManager(int width, int height, Container c) {
		if (width <= 0 || height <= 0) {
			throw new IllegalArgumentException("Dimensions must be non-negative.");
		}
		engine = new Engine(width, height);
		c.add(engine.getCanvas());
	}
	
	
	public void addState(AbstractState newState) {
		for (AbstractState state : stateList) {
			if (state.getType() == newState.getType()) {
				throw new IllegalArgumentException("State type already exists.");
			}
		}
		if (stateList.isEmpty()) {
			stateList.add(newState);
			setCurrentState(newState.getType());
		} else {
			stateList.add(newState);
		}
	}
	
	
	public AbstractState getState(StateType type) {
		for (AbstractState state : stateList) {
			if (state.getType() == type) {
				return state;
			}
		}
		return null;
	}
	
	
	public void setCurrentState(StateType type) {
		for (AbstractState state : stateList) {
			if (state.getType() == type) {
				currentState = type;
				engine.setState(state);
			}
		}
	}
	
	
	public void startCurrentState() {
		engine.start();
	}
	
	
	public void stopCurrentState() {
		engine.stop();
	}
	
	
	public StateType getCurrentState() {
		return currentState;
	}
	
	
	public Canvas getCanvas() {
		return engine.getCanvas();
	}
	
	
	public long getTickCount() {
		return engine.getTickCount();
	}
	
	
	public boolean requestFocus() {
		return engine.getCanvas().requestFocusInWindow();
	}
	
}
