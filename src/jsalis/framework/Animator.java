/**
 * Provides linear animation for a list of BufferedImages.
 * Animator encapsulates the image list and keeps track of the
 * current frame.
 * 
 * @author John Salis
 */

package jsalis.framework;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Animator {
	
	private ArrayList<BufferedImage> frameList;
	
	private volatile boolean isEnabled = false;
	private volatile boolean isRepeating = true;
	private volatile boolean isReversed = false;
	private int startFrame = 0, currentFrame = 0, endFrame;
	private int count = 0;
	private int tickRate;
	
	public Animator(ArrayList<BufferedImage> frames, int tickRate) {
		this.frameList = frames;
		this.tickRate = tickRate;
		this.endFrame = frames.size() - 1;
	}
	
	
	public Animator(ArrayList<BufferedImage> frames) {
		this(frames, 1);
	}
	
	
	public boolean tick() {
		if (isEnabled) {
			if (isReversed) {
				count --;
				if (count / tickRate == startFrame - 1) {
					count = startFrame * tickRate;
					if (!isRepeating) isEnabled = false;
				}
			} else {
				count ++;
				if (count / tickRate == endFrame + 1) {
					count = startFrame * tickRate;
					if (!isRepeating) isEnabled = false;
				}
			}
			currentFrame = (int) Math.floor((double)count / tickRate);
			return true;
		}
		return false;
	}
	
	
	public void setEnabled(boolean b) {
		isEnabled = b;
	}
	
	
	public boolean isEnabled() {
		return isEnabled;
	}
	
	
	public void setRepeating(boolean b) {
		isRepeating = b;
	}
	
	
	public boolean isRepeating() {
		return isRepeating;
	}
	
	
	public void setReversed(boolean b) {
		isReversed = b;
	}
	
	
	public boolean isReversed() {
		return isReversed;
	}
	
	
	public void reset() {
		isEnabled = false;
		count = 0;
		currentFrame = 0;
	}
	
	
	public boolean setTickRate(int n) {
		if (n >= 1) {
			tickRate = n;
			return true;
		} else return false;
	}
	
	
	public int getTickRate() {
		return tickRate;
	}
	
	
	public boolean setStartFrame(int n) {
		if (n >= 0 && n < frameList.size() && n < endFrame) {
			startFrame = n;
			return true;
		} else return false;
	}
	
	
	public boolean setEndFrame(int n) {
		if (n > 0 && n < frameList.size() && n > startFrame) {
			startFrame = n;
			return true;
		} else return false;
	}
	
	
	public boolean setFrames(ArrayList<BufferedImage> frames) {
		if (this.frameList.size() == frames.size()) {
			this.frameList = frames;
			return true;
		} else return false;
	}
	
	
	public BufferedImage getSprite() {
		return frameList.get(currentFrame);
	}
	
	
	public int getCurrentFrame() {
		return currentFrame;
	}
	
}
