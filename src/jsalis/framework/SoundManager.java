/**
 * Manages the state of Sound objects.
 * Loads sound files from the default audio directory
 * specified by the ResourceLoader. Specific sound files
 * can be added manually, but only (.wav) is supported.
 * 
 * @author John Salis
 */

package jsalis.framework;

import java.io.IOException;
import java.util.ArrayList;

import loader.ResourceLoader;

public class SoundManager {
	
	public final static String WAV = "wav";
	private ArrayList<Sound> soundList = new ArrayList<Sound>();
	
	public SoundManager() {}
	
	
	public void loadSounds() {
		loadSounds("");
	}
	
	
	public void loadSounds(String path) {
		try {
			ArrayList<String> fileList = ResourceLoader.getFileList(ResourceLoader.AUDIO, path);
			for (String name : fileList) {
				addSound(name);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void addSound(String filePath) {
		int i = filePath.lastIndexOf(".");
		if (i > 0) {
			String ext = filePath.substring(i + 1);
			if (ext.equals(WAV)) {
				soundList.add(new Sound(filePath));
			}
		}
	}
	
	
	public void playSound(String name) {
		for (Sound s : soundList) {
			if (s.getName().equals(name)) {
				s.play();
				return;
			}
		}
	}
	
	
	public void loopSound(String name) {
		for (Sound s : soundList) {
			if (s.getName().equals(name)) {
				s.loop();
				return;
			}
		}
	}
	
	
	public void stopSound(String name) {
		for (Sound s : soundList) {
			if (s.getName().equals(name)) {
				s.stop();
				return;
			}
		}
	}
	
	
	public void stopAllSounds() {
		for (Sound s : soundList) {
			s.stop();
		}
	}
	
}
