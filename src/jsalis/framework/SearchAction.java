/**
 * @author John Salis
 */

package jsalis.framework;

public class SearchAction {

	public int state;
	public float cost;


	public SearchAction(int state, float cost) {
		this.state = state;
		this.cost = cost;
	}
}