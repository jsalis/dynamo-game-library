/**
 * Manages the input from the mouse and keyboard.
 * Listens for events from components and updates the pressed 
 * state of InputMap(s). Also keeps track of mouse information.
 * 
 * @author John Salis
 */

package jsalis.framework;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

public class InputManager implements KeyListener, MouseListener, MouseMotionListener {
	
	private class Mouse {
		private int x, y;
		private boolean inScreen = false;
		private boolean isDragging = false;
		private Mouse() {}
	}
	
	private Mouse MOUSE = new Mouse();
	private ArrayList<InputMap> mapList = new ArrayList<InputMap>();
	
	public InputManager(Component c) {
		c.addKeyListener(this);
		c.addMouseListener(this);
		c.addMouseMotionListener(this);
	}
	
	
	public void addMap(Action action, int keyCode) {
		mapList.add(new InputMap(action, keyCode));
	}
	
	
	public void removeMap(Action action) {
		for (int i=0; i < mapList.size(); i++) {
			if (mapList.get(i).getAction() == action) {
				mapList.remove(i);
			}
		}
	}
	
	
	public boolean isPressed(Action action) {
		for (InputMap map : mapList) {
			if (map.getAction() == action) {
				return map.isPressed();
			}
		}
		return false; // Some error should be created if map is not found
	}
	
	
	public int getMouseX() {
		return MOUSE.x;
	}
	
	
	public int getMouseY() {
		return MOUSE.y;
	}
	
	
	public boolean mouseInScreen() {
		return MOUSE.inScreen;
	}
	
	
	public boolean mouseIsDragging() {
		return MOUSE.isDragging;
	}
	
	//////////////// Event Listeners ////////////////
	
	public void keyPressed(KeyEvent e) {
		for (InputMap map : mapList) {
			if (e.getKeyCode() == map.getCode()) {
				map.setPressed(true);
			}
		}
	}

	
	public void keyReleased(KeyEvent e) {
		for (InputMap map : mapList) {
			if (e.getKeyCode() == map.getCode()) {
				map.setPressed(false);
			}
		}
	}

	
	public void keyTyped(KeyEvent e) {
		// TODO
	}


	public void mouseDragged(MouseEvent e) {
		MOUSE.isDragging = true;
		MOUSE.x = e.getX();
		MOUSE.y = e.getY();
	}


	public void mouseMoved(MouseEvent e) {
		MOUSE.isDragging = false;
		MOUSE.x = e.getX();
		MOUSE.y = e.getY();
	}


	public void mouseEntered(MouseEvent e) {
		MOUSE.inScreen = true;
	}


	public void mouseExited(MouseEvent e) {
		MOUSE.inScreen = false;
	}
	
	
	public void mousePressed(MouseEvent e) {
		for (InputMap map : mapList) {
			if (e.getButton() == map.getCode()) {
				map.setPressed(true);
			}
		}
	}


	public void mouseReleased(MouseEvent e) {
		for (InputMap map : mapList) {
			if (e.getButton() == map.getCode()) {
				map.setPressed(false);
			}
		}
	}
	
	
	public void mouseClicked(MouseEvent e) {
		// TODO
	}
	
}
