/**
 * Represents a location and size.
 * Any class that extends Sprite have these properties.
 * 
 * @author John Salis
 */

package jsalis.framework;

import java.awt.Rectangle;

public abstract class Sprite implements Entity {
	
	public double x, y;
	public int width, height;
	
	public void setX(double x) {
		this.x = x;
	}
	
	
	public double getX() {
		return x;
	}
	
	
	public void setY(double y) {
		this.y = y;
	}
	
	
	public double getY() {
		return y;
	}
	
	
	public int getWidth() {
		return width;
	}
	
	
	public int getHeight() {
		return height;
	}
	
	
	public Rectangle getRect() {
		return new Rectangle((int)x, (int)y, width, height);
	}

}
