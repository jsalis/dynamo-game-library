/**
 * A mapping of an action to a input code.
 * Used by InputManager to keep track of which 
 * keys (or other input types) are pressed.
 * 
 * @author John Salis
 */

package jsalis.framework;

public class InputMap {
	
	private Action action;
	private int inputCode;
	private int pressCount = 0;
	private boolean isPressed = false;
	
	public InputMap(Action action, int inputCode) {
		this.action = action;
		this.inputCode = inputCode;
	}
	
	
	public void setPressed(boolean bool) {
		if(bool && !isPressed) {
			pressCount ++;
		}
		isPressed = bool;
	}
	
	
	public Action getAction() {
		return action;
	}
	
	
	public int getCode() {
		return inputCode;
	}
	
	
	public int getPressCount() {
		return pressCount;
	}
	
	
	public boolean isPressed() {
		return isPressed;
	}

}
