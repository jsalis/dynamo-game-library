/**
 * The Searchable interface is used by Pathfinder.
 * A class that implements this interface can use Pathfinder
 * to find a path between two states.
 * 
 * 	@author John Salis
 */

package jsalis.framework;

import java.util.ArrayList;

public interface Searchable {

	public int getStart();

	public int getGoal();

	public boolean testGoal(int state);
	
	public float estimateCostToGoal(int state);

	public ArrayList<SearchAction> getSearchActions(int state);
}
