/**
 * Represents a state within the game.
 * A class that extends AbstractState can be used by
 * a StateManager and controlled by an Engine.
 * 
 * @author John Salis
 */

package jsalis.framework;

public abstract class AbstractState implements Entity {
	
	private StateType type;
	protected StateManager stateManager;
	
	public AbstractState(StateType type, StateManager sm) {
		this.type = type;
		this.stateManager = sm;
	}
	
	
	public StateType getType() {
		return type;
	}
	
	
	public StateManager getStateManager() {
		return stateManager;
	}

}
