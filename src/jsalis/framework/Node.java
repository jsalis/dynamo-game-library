/**
 * Used by Pathfinder to represent a state within a search tree.
 * 
 * @author John Salis
 */

package jsalis.framework;

public class Node {
	
	public int state;
	public float gCost, hCost;
	public Node parent;


	public Node(int state) {
		this.state = state;
		this.gCost = 0;
		this.hCost = 0;
	}
	
	
	public float getFCost() {
		return gCost + hCost;
	}


	public boolean hasParent() {
		if (parent != null) {
			return true;
		} else return false;
	}
}
