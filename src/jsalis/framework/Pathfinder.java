/**
 * Searches for a path through an abstract set of data.
 * Also stores statistics about the last executed search.
 * 
 * @author John Salis
 */

package jsalis.framework;

import java.util.ArrayDeque;
import java.util.ArrayList;

public class Pathfinder {

	private ArrayDeque<Node> frontier;
	private ArrayDeque<Node> explored;
	private int nodesExpanded = 0;
	private float pathCost = 0;


	public Pathfinder() {
	}


	public int getNumNodesExpanded() {
		return explored.size();
	}


	public float getPathCost() {
		return pathCost;
	}


	public ArrayDeque<Integer> findPath(Searchable data) {

		Node start = new Node(data.getStart());
		frontier = new ArrayDeque<Node>();
		explored = new ArrayDeque<Node>();
		pathCost = 0;
		frontier.add(start);

		Node currentNode = null;
		while (!frontier.isEmpty()) {

			currentNode = getLowestCostNode();
			if (data.testGoal(currentNode.state)) {
				pathCost = currentNode.gCost;
				return extractSolution(currentNode);
			}
			explored.add(currentNode);

			ArrayList<SearchAction> actionList = data.getSearchActions(currentNode.state);
			for (SearchAction action : actionList) {

				Node child = new Node(action.state);
				child.gCost = currentNode.gCost + action.cost;
				child.hCost = data.estimateCostToGoal(child.state);
				child.parent = currentNode;
				if (!isRepeated(child.state)) {
					frontier.add(child);
				}
			}
		}
		return null;
	}


	private Node getLowestCostNode() {
		
		Node lowestCostNode = frontier.peek();
		for (Node node : frontier) {
			if (node.getFCost() < lowestCostNode.getFCost()) {
				lowestCostNode = node;
			}
		}
		frontier.remove(lowestCostNode);
		return lowestCostNode;
	}


	private boolean isRepeated(int state) {
		
		for (Node node : frontier) {
			if (node.state == state) {
				return true;
			}
		}
		for (Node node : explored) {
			if (node.state == state) {
				return true;
			}
		}
		return false;
	}


	private ArrayDeque<Integer> extractSolution(Node node) {
		
		ArrayDeque<Integer> path = new ArrayDeque<Integer>();
		Node currentNode = node;
		path.push(currentNode.state);
		while (currentNode.hasParent()) {
			currentNode = currentNode.parent;
			path.push(currentNode.state);
		}
		return path;
	}
}
