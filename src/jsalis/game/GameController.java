package jsalis.game;

import jsalis.framework.StateManager;
import jsalis.framework.StateType;

public class GameController {
	
	private GameView gameView;
	private StateManager stateManager;
	
	public GameController() {
		gameView = new GameView(this);
		stateManager = new StateManager(GameView.WIDTH, GameView.HEIGHT, gameView.getContentPane());
		stateManager.addState(new GameState(StateType.GAME, stateManager));
		stateManager.addState(new MenuState(StateType.MENU, stateManager));
		gameView.start();
		stateManager.startCurrentState();
	}
	
	
	public void setCurrentState(StateType type) {
		stateManager.setCurrentState(type);
	}
	
	
	public void requestFocus() {
		stateManager.requestFocus();
	}
	
}
