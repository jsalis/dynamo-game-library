package jsalis.game;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import jsalis.framework.*;

public class GameState extends AbstractState {
	
	private InputManager inputManager;
	private SoundManager soundManager;
	private GameModel gameModel;

	public GameState(StateType type, StateManager sm) {
		super(type, sm);
		createInputManager();
		soundManager = new SoundManager();
		soundManager.loadSounds();
		gameModel = new GameModel();
	}
	
	
	private void createInputManager() {
		
		inputManager = new InputManager(stateManager.getCanvas());
		inputManager.addMap(Action.UP, KeyEvent.VK_UP);
		inputManager.addMap(Action.DOWN, KeyEvent.VK_DOWN);
		inputManager.addMap(Action.LEFT, KeyEvent.VK_LEFT);
		inputManager.addMap(Action.RIGHT, KeyEvent.VK_RIGHT);
		inputManager.addMap(Action.FIRE, KeyEvent.VK_Z);
		inputManager.addMap(Action.PAUSE, KeyEvent.VK_P);
	}
	
	
	private void checkInput() {
		
		Character player = gameModel.getCharacter();
		
		boolean up = inputManager.isPressed(Action.UP);
		boolean down = inputManager.isPressed(Action.DOWN);
		if (up && !down) {
			player.setYDirection(Direction.POSITIVE);
		} else if (!up && down) {
			player.setYDirection(Direction.NEGATIVE);
		} else {
			player.setYDirection(Direction.IDLE);
		}
		
		boolean left = inputManager.isPressed(Action.LEFT);
		boolean right = inputManager.isPressed(Action.RIGHT);
		if (right && !left) {
			player.setXDirection(Direction.POSITIVE);
		} else if (!right && left) {
			player.setXDirection(Direction.NEGATIVE);
		} else {
			player.setXDirection(Direction.IDLE);
		}
		
		if (inputManager.isPressed(Action.FIRE)) {
			player.fireGun();
		}
		if (inputManager.isPressed(Action.PAUSE)) {
			// TODO
		}
	}
	
	
	public void tick() {
		checkInput();
		gameModel.tick();
	}
	
	
	public void render(Graphics2D g) {
		gameModel.render(g);
	}

}
