
package jsalis.game;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import jsalis.framework.*;

public class MenuState extends AbstractState {
	
	private InputManager inputManager;
	private SoundManager soundManager;

	public MenuState(StateType type, StateManager sm) {
		super(type, sm);
		inputManager = new InputManager(stateManager.getCanvas());
		soundManager = new SoundManager();
	}
	
	
	private void checkInput() {
		// TODO
	}
	
	
	public void tick() {
		checkInput();
	}
	
	
	public void render(Graphics2D g) {
		// TODO
	}

}
