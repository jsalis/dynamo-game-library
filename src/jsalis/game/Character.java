package jsalis.game;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import jsalis.framework.Animator;
import jsalis.framework.Sprite;
import loader.ResourceLoader;

public class Character extends Sprite {
	
	public static final double RADIAN_45 = Math.sqrt(2) / 2;
	private float velocityX = 0;
	private float velocityY = 0;
	private float acceleration = 0.9f;
	private float friction = 0.75f;
	
	private boolean movingDiagonal = false;
	private boolean facingLeft = false;
	private Direction xDir = Direction.IDLE;
	private Direction yDir = Direction.IDLE;
	
	public static final int X_OFFSET = 22;
	public static final int Y_OFFSET = 42;
	private ArrayList<BufferedImage> spriteR, spriteL;
	private Animator animator;
	private Gun gun;
	
	public Character() {
		
		loadSprite("character_walk.gif");
		
		this.width = spriteL.get(0).getWidth() - X_OFFSET;
		this.height = spriteL.get(0).getHeight() + Y_OFFSET;
		this.x = (GameView.WIDTH / 2) - (this.width / 2);
		this.y = (GameView.HEIGHT / 2) - (this.height / 2);
		
		gun = new Gun(GunType.PISTOL);
	}
	
	
	private void loadSprite(String path) {
		
		try {
			spriteR = ResourceLoader.getImageArray(path, 11, 64, 18);;
		} catch (IOException e) {
			e.printStackTrace();
		}
		spriteL = ResourceLoader.flipImageArrayHorizontal(spriteR);
		animator = new Animator(spriteL, 3);
		animator.setStartFrame(1);
	}
	
	
	public float getVelocityX() {
		return velocityX;
	}
	
	
	public void setVelocityX(float n) {
		velocityX = n;
	}
	
	
	public float getVelocityY() {
		return velocityY;
	}
	
	
	public void setVelocityY(float n) {
		velocityY = n;
	}
	
	
	public boolean setAcceleration(float a) {
		if (a >= 0) {
			acceleration = a;
			return true;
		} else return false;
	}
	
	
	public double getAcceleration() {
		return acceleration;
	}
	
	
	public void setXDirection(Direction x) {
		xDir = x;
		if (xDir == Direction.NEGATIVE) facingLeft = true;
		if (xDir == Direction.POSITIVE) facingLeft = false;
	}
	
	
	public void setYDirection(Direction y) {
		yDir = y;
	}
	
	
	public void fireGun() {
		gun.fire();
	}
	
	
	public void tick() {
		
		switch (xDir) {
		case NEGATIVE:
			velocityX -= acceleration;
			break;
		case POSITIVE:
			velocityX += acceleration;
			break;
		}
		switch (yDir) {
		case NEGATIVE:
			velocityY += acceleration;
			break;
		case POSITIVE:
			velocityY -= acceleration;
			break;
		}
		if (xDir != Direction.IDLE && yDir != Direction.IDLE && !movingDiagonal) {
			movingDiagonal = true;
			velocityX *= RADIAN_45;
			velocityY *= RADIAN_45;
			acceleration *= RADIAN_45;
		} else if ((xDir == Direction.IDLE || yDir == Direction.IDLE) && movingDiagonal) {
			movingDiagonal = false;
			velocityX /= RADIAN_45;
			velocityY /= RADIAN_45;
			acceleration /= RADIAN_45;
		}
		velocityX *= friction;
		velocityY *= friction;
		if (velocityX < 0.01 && velocityX > -0.01) velocityX = 0;
		if (velocityY < 0.01 && velocityY > -0.01) velocityY = 0;
		this.x += velocityX;
		this.y += velocityY;
		
		gun.setX(this.x);
		gun.setY(this.y);
		gun.setFacingLeft(facingLeft);
		gun.tick();
	}
	
	
	public void tickAnimation() {
		if ((xDir == Direction.IDLE && yDir == Direction.IDLE) || 
			(velocityX == 0 && velocityY == 0)) {
			animator.reset();
		} else {
			animator.setEnabled(true);
		}
		animator.tick();
	}
	
	
	public void render(Graphics2D g) {
		
		int i = animator.getCurrentFrame();
		if (facingLeft) {
			g.drawImage(spriteL.get(i), (int)x - X_OFFSET, (int)y + Y_OFFSET, null);
		} else {
			g.drawImage(spriteR.get(i), (int)x, (int)y + Y_OFFSET, null);
		}
		gun.render(g);
	}
	
	
	public void setX(double x) {
		super.setX(x);
		gun.setX(x);
	}
	
	
	public void setY(double y) {
		super.setY(y);
		gun.setY(y);
	}

}
