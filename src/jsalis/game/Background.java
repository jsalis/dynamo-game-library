
package jsalis.game;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import jsalis.framework.Sprite;
import loader.ResourceLoader;

public class Background extends Sprite {
	
	private ArrayList<Layer> layerList = new ArrayList<Layer>();
	
	private class Layer {
		private BufferedImage image;
		private float x, y;
		private Layer(BufferedImage image, int x, int y) {
			this.image = image;
			this.x = x;
			this.y = y;
		}
	}
	
	public Background() {
		try {
			/* add background layers from back to front */
			layerList.add(new Layer(ResourceLoader.getImage("bg_street.gif"), 0, 0));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void tick() {
		// TODO
	}
	
	
	public void render(Graphics2D g) {
		for (Layer layer : layerList) {
			g.drawImage(layer.image, (int) layer.x, (int) layer.y, null);
		}
	}
	
	
	public void moveHorizontal(float n) {
		
		float count = 0;
		float depth = layerList.size() - 1;
		for (Layer layer : layerList) {
			if (depth == 0) {
				layer.x += n;
				return;
			}
			layer.x += n * (count / depth);
			count ++;
		}
	}
	
	
	public void moveVertical(float n) {
		
		float count = 0;
		float depth = layerList.size() - 1;
		for (Layer layer : layerList) {
			if (depth == 0) {
				layer.y += n;
				return;
			}
			layer.y += n * (count / depth);
			count ++;
		}
	}

}
