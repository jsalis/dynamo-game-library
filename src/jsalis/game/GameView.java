package jsalis.game;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

public class GameView extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	public final static String TITLE = "Garden Guard";
	public static final int WIDTH = 720;
	public static final int HEIGHT = 540;
	
	private GameController gameController;
	
	public GameView(GameController gc) {
		
		gameController = gc;
		
		setTitle(TITLE);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setFocusable(true);
		
		/* make gameController get the focus whenever the frame is activated */
		addWindowFocusListener(new WindowAdapter() {
			public void windowGainedFocus(WindowEvent e) {
				gameController.requestFocus();
			}
		});
	}


	public void start() {
		pack();
		validate();
		setLocationRelativeTo(null);
		setVisible(true);
	}

}