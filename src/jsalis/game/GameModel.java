
package jsalis.game;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class GameModel {
	
	private Character player;
	private Rectangle playerBounds;
	private Background background;
	
	public GameModel() {
		player = new Character();
		playerBounds = new Rectangle(new Dimension(360, 360));
		playerBounds.x = (GameView.WIDTH / 2) - (playerBounds.width / 2);
		playerBounds.y = (GameView.HEIGHT / 2) - (playerBounds.height / 2) + 60;
		background = new Background();
	}
	
	
	public void testBounds() {
		
		/* if player position hits left screen limit */
		if (player.getX() < playerBounds.getX()) {
			player.setX(playerBounds.getX());
			background.moveHorizontal(-player.getVelocityX());
			
		/* if player position hits right screen limit */
		} else if (player.getX() + player.getWidth() > playerBounds.getMaxX()) {
			player.setX(playerBounds.getMaxX() - player.getWidth());
			background.moveHorizontal(-player.getVelocityX());
		}
		
		/* if player position hits top screen limit */
		if (player.getY() < playerBounds.getY()) {
			player.setY(playerBounds.getY());;
			player.setVelocityY(0);
			
		/* if player position hits bottom screen limit */
		} else if (player.getY() + player.getHeight() > playerBounds.getMaxY()) {
			player.setY(playerBounds.getMaxY() - player.getHeight());
			player.setVelocityY(0);
		}
		
	}
	
	
	public void tick() {
		player.tick();
		testBounds();
		player.tickAnimation();
	}
	
	
	public void render(Graphics2D g) {
		background.render(g);
		player.render(g);
		// g.setColor(Color.BLACK);
		// g.setStroke(new BasicStroke(1));
		// g.drawRect(playerBounds.x, playerBounds.y, playerBounds.width, playerBounds.height);
	}
	
	
	public Character getCharacter() {
		return player;
	}

}
