
package jsalis.game;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import jsalis.framework.Sprite;
import loader.ResourceLoader;

public class Bullet extends Sprite {
	
	private static BufferedImage sprite;
	private float velocityX, velocityY;
	private int lifeTime;
	private boolean isDead = false;
	
	public Bullet(double x, double y, float velocityX, float velocityY, int lifeTime) {
		this.x = x;
		this.y = y;
		this.velocityX = velocityX;
		this.velocityY = velocityY;
		this.lifeTime = lifeTime;
		try {
			sprite = ResourceLoader.getImage("bullet.gif");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	public void tick() {
		this.x += velocityX;
		this.y += velocityY;
		lifeTime --;
		if (lifeTime == 0) {
			isDead = true;
		}
	}

	
	public void render(Graphics2D g) {
		g.drawImage(sprite, (int)x, (int)y, null);
	}
	
	
	public boolean isDead() {
		return isDead;
	}

}
