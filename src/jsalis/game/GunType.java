package jsalis.game;

public enum GunType {
	
	PISTOL	(4, 3, 3, 3, 6, 3, FireType.SINGLE),
	SMG		(0, 0, 0, 0, 0, 0, FireType.AUTO),
	SNIPER	(0, 0, 0, 0, 0, 0, FireType.SINGLE),
	SHOTGUN	(0, 0, 0, 0, 0, 0, FireType.SINGLE),
	AK47	(0, 0, 0, 0, 0, 0, FireType.AUTO),
	MINIGUN	(0, 0, 0, 0, 0, 0, FireType.AUTO);
	
	public static final int MAX_STAT = 10;
	private int damage, range, fireRate, accuracy, mobility, melee;
	private FireType fireType;
	
	public enum FireType {
		SINGLE, AUTO, BURST
	}
	
	GunType(int damage, int range, int fireRate, int accuracy, 
			int mobility, int melee, FireType fireType) {
		this.damage = damage;
		this.range = range;
		this.fireRate = fireRate;
		this.accuracy = accuracy;
		this.mobility = mobility;
		this.melee = melee;
		this.fireType = fireType;
	}
	
	
	public int getDamage() {
		return damage;
	}
	
	
	public boolean upDamage() {
		if (damage < MAX_STAT) {
			damage ++;
			return true;
		} else return false;
	}
	
	
	public int getRange() {
		return range;
	}
	
	
	public boolean upRange() {
		if (range < MAX_STAT) {
			range ++;
			return true;
		} else return false;
	}
	
	
	public int getFireRate() {
		return fireRate;
	}
	
	
	public boolean upFireRate() {
		if (fireRate < MAX_STAT) {
			fireRate ++;
			return true;
		} else return false;
	}
	
	
	public int getAccuracy() {
		return accuracy;
	}
	
	
	public boolean upAccuracy() {
		if (accuracy < MAX_STAT) {
			accuracy ++;
			return true;
		} else return false;
	}
	
	
	public int getMobility() {
		return mobility;
	}
	
	
	public boolean upMobility() {
		if (mobility < MAX_STAT) {
			mobility ++;
			return true;
		} else return false;
	}
	
	
	public int getMelee() {
		return melee;
	}
	
	
	public boolean upMelee() {
		if (melee < MAX_STAT) {
			melee ++;
			return true;
		} else return false;
	}
	
	
	public FireType getFireType() {
		return fireType;
	}
	
}
