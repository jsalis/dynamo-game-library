package jsalis.game;
import java.awt.Graphics2D;

import jsalis.framework.Sprite;


public class Zombie extends Sprite {
	
	public static final int MAX_HEALTH = 100;
	private int health;
	
	public Zombie() {
		
		health = MAX_HEALTH;
	}
	
	
	public void lowerHealth(int n) {
		
		if (health - n >= 0) {
			health -= n;
		} else {
			health = 0;
		}
	}
	
	
	public void raiseHealth(int n) {
		
		if (health + n <= MAX_HEALTH) {
			health += n;
		} else {
			health = MAX_HEALTH;
		}
	}
	
	
	public int getHealth() {
		return health;
	}


	public void tick() {
		
	}


	public void render(Graphics2D g) {
		
	}

}
